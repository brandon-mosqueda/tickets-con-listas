using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Test
{
    class Producto
    {
       public string Nombre { get; }
       public int Cantidad { get; }
       public float Costo { get; }
       public float Importe { get { return Cantidad * Costo; } }

        public Producto (string _Nombre, int _Cantidad, float _Costo)
        {
            Nombre = _Nombre;
            Cantidad = _Cantidad;
            Costo = _Costo;
        }
    }
}