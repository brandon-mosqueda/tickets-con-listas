using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Test
{
    class GeneradorImpresionTicket
    {
    	//#   Nombre    Cantidad Precio Subtotal
    	private static readonly int MIN_CARACTERES_POR_LINEA = 35;
    	//Tú puedes editar esta variable para hacer tickets impresos en formatos más anchos
    	private static readonly int MAX_CARACTERES_POR_LINEA = 50; //Mínimo 36

    	public static string Generar(Ticket ticket)
    	{
	    	string ticketString = ObtenerEncabezado();

	    	for(int i= 0; i < ticket.Productos.Count; i++)
	    	{
	    	    ticketString += ObtenerProductoResumen(
		    	    				i + 1, ticket.Productos.ElementAt(i)
		    	    			);
	    	 
	    	    ticketString += '\n';
	    	}

	    	ticketString += "\n\n";
	    	ticketString += ObtenerFooter(ticket);

    	    return ticketString;
    	}

    	private static string ObtenerEncabezado()
    	{
    		string espaciosDespuesNombre = ObtenerEspacios(
				    							MAX_CARACTERES_POR_LINEA - 
				    							MIN_CARACTERES_POR_LINEA
			    							);
    		return "#   Nombre" + espaciosDespuesNombre + "Cantidad Precio Subtotal\n\n";
    	}

    	private static string ObtenerEspacios(int cantidadEspacios)
    	{
    		string espacios = "";

    		for(int i = 0; i < cantidadEspacios; i++)
    			espacios += " ";

    		return espacios;
    	}

    	private static string ObtenerProductoResumen(int indice, Producto producto)
    	{
    		string nombre = CorregirNombreProductoParaTicket(producto.Nombre);

    		string espaciosIndice = ObtenerEspacios(4 - indice.ToString().Length);
    		string espaciosNombre = ObtenerEspacios(
    									ObtnerTamanioMaximoNombreProducto() -
    									nombre.Length
    								);
    		string espaciosCantidad = ObtenerEspacios(8 - producto.Cantidad.ToString().Length);
    		string espaciosCosto = ObtenerEspacios(7 - producto.Costo.ToString().Length);

    		return indice + espaciosIndice + 
    			   nombre + espaciosNombre + "  " +
    			   producto.Cantidad + espaciosCantidad + 
    			   "$" + producto.Costo + espaciosCosto + 
    			   "$" + producto.Importe;
    	}

    	private static string CorregirNombreProductoParaTicket(string nombre)
    	{
    		string nombreCorregido = nombre;
    		int tamanioMaximoNombre = ObtnerTamanioMaximoNombreProducto();

    		if(nombre.Length > tamanioMaximoNombre)
    			nombreCorregido = nombre.Substring(0, tamanioMaximoNombre - 3) + "...";

    		return nombreCorregido;
    	}

    	private static int ObtnerTamanioMaximoNombreProducto()
    	{
    		//El 6 es por la palabra "Nombre" en el encabezado
    		return MAX_CARACTERES_POR_LINEA - MIN_CARACTERES_POR_LINEA + 6;
    	}

    	private static string ObtenerFooter(Ticket ticket)
    	{
    		string subtotal = "Subtotal: $" + ticket.Subtotal;
    		string impuesto = "Impuesto: $" + ticket.Impuesto;
    		string total =    "Total:    $" + ticket.CostoTotal;
    		string pagado =   "Pagado:   $" + ticket.Pago;
    		string cambio =   "Cambio:   $" + ticket.Cambio;

    		return subtotal + '\n' +
    			   impuesto + '\n' +
    			   total + "\n\n" +
    			   pagado + '\n' +
    			   cambio + '\n';
    	}
    }
}