Programa en C# de ventas (clase ticket) que almacena productos y genera el 
formato con el resumen de la venta, los productos, subtotal, impuesto, lo 
pagado y el cambio para que se pueda imprimir.

![Ejemplo reporte](Ejemplo_reporte.png)