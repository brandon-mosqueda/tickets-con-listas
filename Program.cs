﻿using System;

namespace Test
{
    class Program
    {
        static Ticket ticket;

        static void Main(string[] args)
        {
         	ticket = new Ticket();

         	AgregarProductosDePrueba();

         	ticket.Pago = 300;

         	string reporte = GeneradorImpresionTicket.Generar(ticket);

         	Console.Write(reporte);
        }

        static void AgregarProductosDePrueba()
        {
    		Producto[] productos = new Producto[] 
    		{
    			new Producto("Gallina", 5, 15f),
    			new Producto("Gallo", 4, 18f),
    			new Producto("Galletas con lehce", 2, 20f),
    			new Producto("Nombre de producto extremadamente largo", 2, 20f)
    		};

    		for(int i = 0; i < productos.Length; i++)
        	 	ticket.AgregarProducto(productos[i]);
        }
    }
}
