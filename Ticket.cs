using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Test
{
    class Ticket
    {
        private const int IVA = 16;

        private List<Producto> _Productos;
        private float _Pago;
        private float _Subtotal;

        public List<Producto> Productos { get { return _Productos; } }
        public float Subtotal { get { return _Subtotal; } }
        public float Impuesto { get { return Subtotal * (IVA / (float)100); } }
        public float CostoTotal { get { return Subtotal + Impuesto; } }
        public float Pago {
            get { return _Pago; }
            set
            {
                if (value >= CostoTotal)
                    _Pago = value;
                else
                    Console.Write("El pago es insuficiente");              
            }
        }
        public float Cambio { get { return Pago - CostoTotal; } }

        
        public Ticket()
        {
            _Productos = new List<Producto>();
        }

        public void AgregarProducto(Producto producto)
        {
            _Productos.Add(producto);
            _Subtotal += producto.Importe;
        }      
    }
}